export namespace brackets {
	/** Collection of all pairs of brackets */
	export const collection = {
		curly: {
			tight: "{}" as "{}",
			spaced: "{ }" as "{ }",
		},
		round: {
			tight: "()" as "()",
			spaced: "( )" as "( )",
		},
		square: {
			tight: "[]" as "[]",
			spaced: "[ ]" as "[ ]",
		},
		angular: {
			tight: "<>" as "<>",
			spaced: "< >" as "< >",
		},
	};

	export type Collection = typeof collection;

	export type Name = keyof Collection;
	export type Kind = keyof Collection[Name];
	export type Brackets = Collection[Name][Kind];

	/**
	 * Surround string by a pair of brackets
	 * @arg input String to enclose
	 * @arg brackets (defaults to `"()"`) Pair of brackets
	 */
	export const surround = (input: any, brackets: Brackets = collection.round.tight): string => {
		const opening = brackets[0];
		const closing = brackets[brackets.length - 1];
		const spacing = brackets.length === 3 && brackets[1] === " "? " " : "";

		return opening + spacing + String(input) + spacing + closing;
	};

	/**
	 * Get surrounder function
	 * @arg brackets Pair of brackets
	 */
	export const surroundBy = (brackets: Brackets) => (input: any): string =>
		surround(input, brackets);
}
